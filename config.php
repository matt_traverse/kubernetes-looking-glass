<?php

/*
 * Main configuration file example.
 *
 * DO NOT EDIT NOR RENAME, please copy to 'config.php' and edit the new file!
 */

// People to contact
// Set both to null to not display any contact information
$config['contact']['name'] = null;
$config['contact']['mail'] = null;

// Frontpage configuration

// Title of the page
$config['frontpage']['title'] = 'Kubernetes Cluster Looking Glass';
// Logo to display (remove it to not display any logo)
//$config['frontpage']['image'] = 'logo.png';
// Disclaimer to inform people using the looking glass
// Set it to null to not display a disclaimer
$config['frontpage']['disclaimer'] = 'Demonstration Purposes Only';

// Things to remove from the output (PHP compatible regex)
$config['filters']['output'][] = '/(client1|client2)/';
$config['filters']['output'][] = '/^NotToShow/';
// Matched patterns can also replaced inline
$config['filters']['output'][] = ['/replacethis/', 'withthis'];

// If telnet is used in combination with extreme_netiron, uncomment the following filter
//$config['filters']['output'][] = '/([^\x20-\x7E]|User|Please|Disable|telnet|^\s*$)/';

// Google reCaptcha integration
$config['recaptcha']['enabled'] = false;
$config['recaptcha']['apikey'] = 'foobar';
$config['recaptcha']['secret'] = 'foobar';

$lg_discovery_host = file_get_contents("/var/www/html/lg_service_host.txt");

$lgendpoints = dns_get_record(trim($lg_discovery_host), DNS_A);
$lgips = array();

foreach($lgendpoints as $returnrec) {
		$ip = $returnrec["ip"];
			array_push($lgips, $ip);
}
sort($lgips);

foreach($lgips as $ip) {
	$config['routers'][$ip]['host'] = $ip;
	$config["routers"][$ip]['user'] = 'lguser';
	$config['routers'][$ip]['auth'] = 'ssh-key';
	$config['routers'][$ip]['private_key'] = '/run/userkeydir/id_rsa';
	$config['routers'][$ip]['type'] = 'linux';
	$config['routers'][$ip]['desc'] = 'Kubernetes pod ' . $ip;
}

// If running on *BSD, disable '-A' which is non-existent
$config['tools']['ping_options'] = '-c 5';
// If running on *BSD, disable '-N' which is non-existent
$config['tools']['traceroute_options'] = '';
// If running on *BSD, there is no '-4' or '-6'
$config['tools']['traceroute6'] = 'tracepath';
$config['tools']['traceroute4'] = 'tracepath';

$config['tools']['iperf3-dl'] = 'iperf3';
$config['tools']['iperf3-ul'] = 'iperf3';

$config['iperf3']['endpoint'] = 'iperf3.lookingglass.svc.cluster.local';

// End of config.php
